package lab5.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import lab5.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class addEmployeeValid {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps user;

    @Test
    public void addValidEmployee() {
        user.is_the_home_page();
        user.looks_for("Alex", "Marian", "1890909121234", "teacher", "3000");
        user.should_see_message_response("SUCCESS!");
    }
} 