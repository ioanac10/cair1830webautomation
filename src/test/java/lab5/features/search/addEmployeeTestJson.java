package lab5.features.search;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import net.thucydides.junit.runners.ThucydidesParameterizedRunner;
import org.junit.runner.RunWith;
import net.thucydides.core.annotations.Test;
import net.thucydides.core.annotations.DataProvider;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@RunWith(ThucydidesParameterizedRunner.class)
public class addEmployeeTestJson {

    @DataProvider
    public Object[][] getData() throws FileNotFoundException {
        JsonElement jsonData = new JsonParser().parse(new FileReader("src/test/resources/45146523.json"));
        JsonElement dataSet = jsonData.getAsJsonObject().get("dataSet");
        List<TestData> testData = new Gson().fromJson(dataSet, new TypeToken<List<TestData>>() {
        }.getType());
        Object[][] returnValue = new Object[testData.size()][1];
        int index = 0;
        for (Object[] each : returnValue) {
            each[0] = testData.get(index++);
        }
        return returnValue;
    }

    @Test(dataProvider = "getData")
    public void testMethod(TestData data) {
        System.out.println(data);
    }
}
