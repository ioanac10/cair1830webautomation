package lab5.features.search;

import lab5.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import net.thucydides.junit.runners.ThucydidesParameterizedRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(ThucydidesParameterizedRunner.class)
@UseTestDataFrom(value="src\\test\\resources\\testData.csv", separator=';')
public class addEmployeeTestCsv {

    private String firstName;
    private String lastName;
    private String cnp;
    private String function;
    private String salary;
    private String result;

    @Managed
    public WebDriver webdriver;

    @Steps
    public EndUserSteps user;

    @Qualifier
    public String getQualifier() {
        return firstName;
    }

    @Test
    public void addValidEmployee() {
        user.is_the_home_page();
        user.looks_for(getFirstName(), getLastName(), getCnp(), getFunction(), getSalary());
        user.should_see_message_response(getResult());
    }


    public String getCnp() {
        return cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFunction() {
        return function;
    }

    public String getLastName() {
        return lastName;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getSalary() {
        return salary;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
