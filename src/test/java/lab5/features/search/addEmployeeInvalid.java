package lab5.features.search;

import lab5.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class addEmployeeInvalid {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps user;

    @Test
    public void addValidEmployee() {
        user.is_the_home_page();
        user.looks_for("Alex", "Marian", "1890909121234", "altceva", "3000");
        user.should_see_message_response("ERROR!");
    }
} 