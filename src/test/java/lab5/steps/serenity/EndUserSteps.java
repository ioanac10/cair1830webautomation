package lab5.steps.serenity;

import lab5.pages.HomeAddEmployeePage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class EndUserSteps {

    HomeAddEmployeePage homeAddEmployeePage;

    @Step
    public void enter_firstName(String keyword) {
        homeAddEmployeePage.enter_firstName(keyword);
    }

    @Step
    public void enter_lastName(String keyword) {
        homeAddEmployeePage.enter_lastName(keyword);
    }

    @Step
    public void enter_cnp(String keyword) {
        homeAddEmployeePage.enter_cnp(keyword);
    }

    @Step
    public void enter_function(String keyword) {
        homeAddEmployeePage.enter_function(keyword);
    }

    @Step
    public void enter_salary(String keyword) {
        homeAddEmployeePage.enter_salary(keyword);
    }

    @Step
    public void send_form() {
        homeAddEmployeePage.sent_form_data();
    }

    @Step
    public void should_see_message_response(String message) {
        Assert.assertEquals(homeAddEmployeePage.getMessage(), message);
    }

    @Step
    public void is_the_home_page() {
        homeAddEmployeePage.open();
    }

    @Step
    public void looks_for(String firstName, String lastName, String cnp, String function, String salary) {
        enter_firstName(firstName);
        enter_lastName(lastName);
        enter_cnp(cnp);
        enter_function(function);
        enter_salary(salary);
        send_form();
    }
}