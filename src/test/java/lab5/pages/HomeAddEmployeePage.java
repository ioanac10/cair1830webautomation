package lab5.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("http://localhost:8080")
public class HomeAddEmployeePage extends PageObject {

    @FindBy(name="firstName")
    private WebElementFacade firstNameField;

    @FindBy(name="lastName")
    private WebElementFacade lastNameField;

    @FindBy(name="cnp")
    private WebElementFacade cnpField;

    @FindBy(name="function")
    private WebElementFacade functionField;

    @FindBy(name="salary")
    private WebElementFacade salaryField;

    @FindBy(name="submit")
    private WebElementFacade submitButton;

    public void enter_firstName(String keyword) {
        firstNameField.type(keyword);
    }

    public void enter_lastName(String keyword) {
        lastNameField.type(keyword);
    }

    public void enter_cnp(String keyword) {
        cnpField.type(keyword);
    }

    public void enter_function(String keyword) {
        functionField.type(keyword);
    }

    public void enter_salary(String keyword) {
        salaryField.type(keyword);
    }

    public void sent_form_data() {
        submitButton.click();
    }

    public String getMessage() {
        WebElementFacade definitionList = find(By.tagName("p"));
        return definitionList.getText();
    }
}